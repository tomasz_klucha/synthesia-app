package horizons.synthesia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SynthesiaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SynthesiaApplication.class, args);
	}
}
