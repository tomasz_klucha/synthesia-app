package horizons.synthesia.db.model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.vividsolutions.jts.geom.Point;

@Entity
public class PointOfInterest {

    @Id
    @GeneratedValue
    private Long id;
    private String type;
    private Point location;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public Point getLocation() {
        return location;
    }

    public void setLocation(Point location) {
        this.location = location;
    }
    
    public String getType() {
        return type;
    }
    
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id, this.location, this.location, this.type);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        final PointOfInterest other = (PointOfInterest) obj;
        return Objects.equals(this.id, other.id)
                && Objects.equals(this.location, other.location)
                && Objects.equals(this.type, other.type);
    }

    @Override
    public String toString() {
        return "POI [id=" + id + ", location=" + location + ", type=" + type + "]";
    }
}
