package horizons.synthesia.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import horizons.synthesia.db.model.PointOfInterest;

public interface PointOfInterestRepository extends JpaRepository<PointOfInterest, Long> {

}
