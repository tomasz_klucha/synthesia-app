package horizons.synthesia.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import horizons.synthesia.db.model.PointOfInterest;
import horizons.synthesia.db.repository.PointOfInterestRepository;

@Component
public class PointOfInterestService {

    @Autowired
    private PointOfInterestRepository repo;

    public void add(PointOfInterest poi) {
        repo.save(poi);
    }

    public List<PointOfInterest> getAll() {
        return repo.findAll();
    }
}
