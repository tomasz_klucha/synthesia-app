package horizons.synthesia.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import horizons.synthesia.db.model.PointOfInterest;
import horizons.synthesia.service.PointOfInterestService;

@RestController
@RequestMapping("/poi")
public class PointOfInterestController {

    @Autowired
    private PointOfInterestService service;
    
    @RequestMapping(method=RequestMethod.POST)
    public void add(@RequestBody PointOfInterest poi) {
        service.add(poi);
    }
    
    @RequestMapping(method=RequestMethod.GET)
    public List<PointOfInterest> getPOIs() {
        List<PointOfInterest> all = service.getAll();
        System.out.println(all);
        return all;
    }
    
}
